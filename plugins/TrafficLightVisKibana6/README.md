# TrafficLightVisKibana6
A traffic lights visualisation panel that can be split on multiple lines

# Traffic Light Visualization

![Transform Vis](https://raw.githubusercontent.com/snuids/TrafficLightVisKibana6/master/pictures/TrafficLights6a.jpg)

## Versions
Kibana 6.X version of http://logz.io/blog/kibana-visualizations/

For Kibana between 5.5 see https://github.com/snuids/TrafficLightVisKibana5.5

For Kibana between 5.0 and 5.4 see https://github.com/snuids/Elastic-5.0-Traffic-Light-Visualizer 

Kibana 4.4 version here https://github.com/snuids/traffic_light_vis


## Options
* Split chart option added. 
* Possibility to define the numer of traffic lights per line.

## Installation
Simply unzip the content in the plugin folder of Kibana.

More info on docker and kibana here: https://mannekentech.com/

