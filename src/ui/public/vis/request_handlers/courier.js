import _ from 'lodash';
import { SearchSourceProvider } from 'ui/courier/data_source/search_source';
import { VisRequestHandlersRegistryProvider } from 'ui/registry/vis_request_handlers';

const CourierRequestHandlerProvider = function (Private, courier, timefilter) {
  const SearchSource = Private(SearchSourceProvider);

  return {
    name: 'courier',
    handler: function (vis, appState, uiState, queryFilter, searchSource) {

      if (queryFilter && vis.editorMode) {
        searchSource.set('filter', queryFilter.getFilters());
        searchSource.set('query', appState.query);
      }

      const shouldQuery = () => {
        for (var filter of searchSource.get('filter')) {
          if (filter.range) {
            for (var prop in filter.range) {
              if (prop === 'creation_historic_time') {
                if (queryFilter && !vis.editorMode) {
                  for (var query of queryFilter.getFilters()) {
                    if (query.meta.key === "creation_year_month.keyword") {
                      const currentDate = new Date();
                      const cMonth = Number(currentDate.getMonth() + 1);
                      const cYear = Number(currentDate.getFullYear());
                      const filterYear = Number(query.meta.value.substring(0, 4));
                      const filterMonth = Number(query.meta.value.substring(5, 7));
                      const propGte = 3;
                      const propLt = 0;
                      if (filterMonth < cMonth && filterYear === cYear) {
                        const diffMonth = cMonth - filterMonth;
                        filter.range[prop].gte = "now" + (-propGte - diffMonth) + "M/M";
                        filter.range[prop].lt = "now" + (-propLt - diffMonth) + "M/M";
                      } else if (filterMonth >= cMonth && filterYear !== cYear) {
                        const diffMonth = filterMonth - cMonth;
                        const diffYear = cYear - filterYear;
                        const gte = -propGte + diffMonth;
                        const lt = -propLt + diffMonth;
                        const yGte = gte < 0 ? "y" : "y+";
                        const yLt =  lt < 0 ? "y" : "y+";
                        filter.range[prop].gte = "now-" + diffYear + yGte + gte + "M/M";
                        filter.range[prop].lt = "now-" + diffYear + yLt + lt + "M/M";
                      } else {
                        filter.range[prop].gte = "now-3M/M";
                        filter.range[prop].lt = "now-0M/M";
                      }
                      break;
                    }
                  }
                }
              }
            }
          }
        }

        if (!searchSource.lastQuery || vis.reload) return true;
        if (!_.isEqual(_.cloneDeep(searchSource.get('filter')), searchSource.lastQuery.filter)) return true;
        if (!_.isEqual(_.cloneDeep(searchSource.get('query')), searchSource.lastQuery.query)) return true;
        if (!_.isEqual(_.cloneDeep(searchSource.get('aggs')()), searchSource.lastQuery.aggs)) return true;
        if (!_.isEqual(_.cloneDeep(timefilter.time), searchSource.lastQuery.time)) return true;

        return false;
      };

      return new Promise((resolve, reject) => {
        if (shouldQuery()) {
          delete vis.reload;
          searchSource.onResults().then(resp => {
            searchSource.lastQuery = {
              filter: _.cloneDeep(searchSource.get('filter')),
              query: _.cloneDeep(searchSource.get('query')),
              aggs: _.cloneDeep(searchSource.get('aggs')()),
              time: _.cloneDeep(timefilter.time)
            };

            searchSource.rawResponse = resp;

            resolve(_.cloneDeep(resp));
          }).catch(e => reject(e));

          courier.fetch();
        } else {
          resolve(_.cloneDeep(searchSource.rawResponse));
        }
      }).then(async resp => {
        for (const agg of vis.getAggConfig()) {
          const nestedSearchSource = new SearchSource().inherits(searchSource);
          resp = await agg.type.postFlightRequest(resp, vis.aggs, agg, nestedSearchSource);
        }
        return resp;
      });
    }
  };
};

VisRequestHandlersRegistryProvider.register(CourierRequestHandlerProvider);

export { CourierRequestHandlerProvider };
